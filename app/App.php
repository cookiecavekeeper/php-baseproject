<?php

class App extends WebApp
{

    /**
     * leitet App ein
     */
    public static function init() {
        define('G_DEBUG', self::getEnvironment() === 'local');
        define('DIR_APP', __DIR__);
        
        if(G_DEBUG) {
            
        } else {
            
        }
        
        self::initRouter();
    }
    
    /**
     * Startet App
     */
    public static function run() {
        self::$slim->run();
    }
    
    /**
     * leitet anhend der URI zu einem Controller und einer Methode weiter
     */
    protected static function initRouter()
    {
        self::$slim = new \Slim\Slim();
        
        //Index
        self::$slim->get('/', function() {
            $page = new IndexController();
            $page->show();
        });
    }
}