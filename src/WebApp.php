<?php

class WebApp
{
    protected static $db = null;
    
    protected static $twig = null;
    
    protected static $slim = null;
    
    protected static $cfg = array();
    
    public static function getDb()
    {
        if(self::$db === null) {
            self::$db = new dbMySql();
            
            return self::$db;
        } else {
            return self::$db;
        }
    }
    
    public static function getTwig()
    {
        if(self::$twig === null) {
            $twigLoader = new Twig_Loader_Filesystem(DIR_APP);
            self::$twig = new Twig_Environment($twigLoader);
            
            return self::$twig;
        } else {
            return self::$twig;
        }
    }
    
    public static function set($strKey, $strValue)
    {
        self::$cfg[$strKey] = $strValue;
    }
    
    public static function get($strKey, $mixDefault)
    {
        if(array_key_exists($strKey, self::$cfg)) {
            return self::$cfg[$strKey];
        } else {
            return $mixDefault;
        }
    }
    
    public static function getEnvironment()
    {
        $arrServer = $_SERVER;
        
        if($arrServer['SERVER_NAME'] === 'localhost' || $arrServer['SERVER_NAME'] === '127.0.0.1') {
            return 'local';
        } else {
            return 'live';
        }
    }
}
